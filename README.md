# Getting Started with Pizza 365

This app aims to create a website to make ordering pizza online easier

## Available Scripts

In the project directory, you can run:

### `npm install`

Ro install all packages for the project

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

### View demo

https://pizza365-fe-react.onrender.com/


## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).


