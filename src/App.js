import "../node_modules/font-awesome/css/font-awesome.min.css"
import 'bootstrap/dist/css/bootstrap.css';
import './App.css';
import Header from "./components/header/Header.component";
import Content from "./components/content/Content.component";
import Footer from "./components/footer/Footer.component";
function App() {
  return (
    <div>
      <Header></Header>
      <Content></Content>
      <Footer></Footer>
    </div>
    
  );
}

export default App;
