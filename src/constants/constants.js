export const CHANGE_NAME = "Change input name"
export const CHANGE_EMAIL = "Change input email"
export const CHANGE_PHONE = "Change input phone"
export const CHANGE_ADDRESS = "Change input address"
export const CHANGE_MESSAGE = "Change input message"
export const CHANGE_VOUCHER = "Change input voucher"
export const CHECK_ORDER = "Click button check order"
export const DRINK_FETCH_PENDING = "Fetch to load drink list PENDING"
export const DRINK_FETCH_SUCCESS = "Fetch to load drink list DONE"
export const DRINK_FETCH_ERROR = "Fetch to load drink list ERROR"
export const CHANGE_DRINK = "change select drink"
export const TYPE_BACON = "choose pizza type Bacon"
export const TYPE_HAWAII = "choose pizza type Hawaii"
export const TYPE_SEAFOOD = "choose pizza type Seafood"
export const SIZE_S = "choose pizza size S"
export const SIZE_M = "choose pizza size M"
export const SIZE_L = "choose pizza size L"
export const VOUCHER_FETCH_SUCCESS = "fetch to search voucher DONE"
export const VOUCHER_FETCH_ERROR = "fetch to search voucher ERROR"
export const VOUCHER_NOT_FOUND = "fetch to search voucher not found"
export const POST_ORDER_SUCCESS = "post order DONE"
export const POST_ORDER_ERROR = "post order ERROR"

export const SmallSizeInfo = {
    kichCo: "S(Small size)",
    duongKinh: 20,
    suon: 2,
    salad: 200,
    soluongNuoc: 2,
    priceVND: 150000
}
export const MediumSizeInfo = {
    kichCo: "M (Medium size)",
    duongKinh: 25,
    suon: 4,
    salad: 300,
    soluongNuoc: 3,
    priceVND: 200000
}
export const LargeSizeInfo = {
    kichCo: "L (Large size)",
    duongKinh: 30,
    suon: 8,
    salad: 500,
    soluongNuoc: 4,
    priceVND: 250000
}








