import {
    CHANGE_ADDRESS, CHANGE_DRINK, CHANGE_EMAIL, CHANGE_MESSAGE, CHANGE_NAME, CHANGE_PHONE, CHANGE_VOUCHER, CHECK_ORDER,
    DRINK_FETCH_ERROR, DRINK_FETCH_PENDING, DRINK_FETCH_SUCCESS, POST_ORDER_ERROR, POST_ORDER_SUCCESS, SIZE_L, SIZE_M, SIZE_S, TYPE_BACON, TYPE_HAWAII, TYPE_SEAFOOD,
    VOUCHER_FETCH_ERROR, VOUCHER_FETCH_SUCCESS, VOUCHER_NOT_FOUND
} from "../constants/constants";

const initialState = {
    size: null,
    type: null,
    drink: null,
    form: {
        name: "",
        email: "",
        phone: "",
        address: "",
        message: "",
        voucher: ""
    },
    pending: false,
}
const pizzaReducer = (state = initialState, action) => {
    switch (action.type) {
        case CHANGE_NAME:
            state.form.name = action.payload
            break;
        case CHANGE_EMAIL:
            state.form.email = action.payload
            break;
        case CHANGE_PHONE:
            state.form.phone = action.payload
            break;
        case CHANGE_ADDRESS:
            state.form.address = action.payload
            break;
        case CHANGE_MESSAGE:
            state.form.message = action.payload
            break;
        case CHANGE_VOUCHER:
            state.form.voucher = action.payload
            break;
        case DRINK_FETCH_PENDING:
            state.pending = true
            break;
        case DRINK_FETCH_SUCCESS:
            state.pending = false
            state.drinkList = action.payload.result
            break;
        case DRINK_FETCH_ERROR:
            break;
        case CHANGE_DRINK:
            state.drink = action.payload
            break;
        case TYPE_BACON:
            state.type = action.payload
            state.styleBacon = true
            state.styleHawaii = false
            state.styleSeafood = false
            break;
        case TYPE_HAWAII:
            state.type = action.payload
            state.styleBacon = false
            state.styleHawaii = true
            state.styleSeafood = false
            break;
        case TYPE_SEAFOOD:
            state.type = action.payload
            state.styleBacon = false
            state.styleHawaii = false
            state.styleSeafood = true
            break;
        case SIZE_S:
            state.size = action.payload
            state.styleS = true
            state.styleM = false
            state.styleL = false
            break;
        case SIZE_M:
            state.size = action.payload
            state.styleS = false
            state.styleM = true
            state.styleL = false
            break;
        case SIZE_L:
            state.size = action.payload
            state.styleS = false
            state.styleM = false
            state.styleL = true
            break;
        case VOUCHER_FETCH_SUCCESS:
            state.discount = action.payload.phanTramGiamGia
            break;
        case VOUCHER_NOT_FOUND:
            state.discount = null
            break;
        case VOUCHER_FETCH_ERROR:
            console.log(action.error)
            break;
        case CHECK_ORDER:
            state.displayCheckOrder = { display: "block" }
            state.checked = true
            state.order = {
                menuDuocChon: state.size.kichCo,
                loaiPizza: state.type,
                hoTen: state.form.name,
                email: state.form.email,
                soDienThoai: state.form.phone,
                diaChi: state.form.address,
                loiNhan: state.form.message,
                idVourcher: state.form.voucher,
                phanTramGiamGia: state.discount,
                giamGia: state.discount * state.size.priceVND / 100,
                idLoaiNuocUong: state.drink,
                thanhTien: state.size.priceVND - state.discount * state.size.priceVND / 100,
                kichCo: state.size.kichCo,
                duongKinh: state.size.duongKinh,
                suon: state.size.suon,
                salad: state.size.salad,
                soLuongNuoc: state.size.soLuongNuoc,
            }
            break;
        case POST_ORDER_SUCCESS:
            state.displayCheckOrder = { display: "none" }
            state.finish = true
            state.code = action.payload.orderCode
            break;
        case POST_ORDER_ERROR:
            console.log(action.error)
            break;
        default:
            break;
    }
    return { ...state }
}
export default pizzaReducer