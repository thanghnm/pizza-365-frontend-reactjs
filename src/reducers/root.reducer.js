import { combineReducers } from "redux";
import pizzaReducer from "./pizza.reducer";
const rootReducer = combineReducers({
    pizzaReducer
})
export default rootReducer