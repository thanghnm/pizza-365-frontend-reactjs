import { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import { changeDrink, fetchDrink } from "../../../action/drink.action"

const Drink =()=> {
  const dispatch =useDispatch()
  const {drinkList} = useSelector((reduxData)=>reduxData.pizzaReducer)
  useEffect(()=>{
    dispatch(fetchDrink())
  },[])
  const onSelectDrinkChangeHandler=(event)=>{
    dispatch(changeDrink(event))
  }
        return(
            <>
            {/* Drink */}
            <div className="drink">
              <label className="drink-label" htmlFor="getdrink">Hãy chọn đồ uống bạn yêu thích</label><br></br>
                <select onChange={onSelectDrinkChangeHandler} className="drink-select" id="getdrink">
                  <option value="0">Chọn đồ uống</option>
                  {drinkList?drinkList.map((element,index)=>{
                    return (
                      <option key={index} value={element.maNuocUong}>{element.tenNuocUong}</option>
                    )
                  }):""}
                </select>
            </div>
            </>
        )
    }

export default Drink