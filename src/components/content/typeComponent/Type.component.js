import { useDispatch, useSelector } from "react-redux"
import { choosePizzaBacon, choosePizzaHawaii, choosePizzaSeafood } from "../../../action/type.action"

const Type =()=>{
  const { styleBacon,styleHawaii,styleSeafood } = useSelector((reduxData) => reduxData.pizzaReducer)
  const dispatch = useDispatch()

  const onBtnTypeHawaiiClick = (event) =>{
    dispatch(choosePizzaHawaii())
  }
  const onBtnTypeSeafoodClick = (event) =>{
    dispatch(choosePizzaSeafood())
  }
  const onBtnTypeBaconClick = (event) =>{
    dispatch(choosePizzaBacon())
  }
        return(
            <>
                     {/* <!-- Title Pizza Type --> */}
                     <div id="type" className="row">
              <div className="col-sm-12 text-center p-4 mt-4">
                <h2><b className="p-2 border-bottom">Chọn loại pizza</b></h2>
              </div>
              {/* <!-- Content Pizza Type --> */}
              <div className="col-sm-12">
                <div className="row" id="row-type">
                  {/* <!-- Call Api --> */}
                  <div className="col-sm-4">
                    <div className="card w-100 type-card">
                      <img src="pizza_type_hawai.jpg" alt="pizza-img" className="card-img-top" />
                      <div id="div-hawai" className="card-body">
                        <h3 id="h3-hawai">Pizza Hawai</h3>
                        <p>Món ăn thanh đạm</p>
                        <p>
                          Hãy thưởng thức món ăn với phong cách Alo Ha đến từ
                          Hawai.
                        </p>
                        <p>
                          <button style={styleHawaii?{background:"orange"}:{background:"#00628f"}} className="btn-green"  id="btn-type-hawai"
                            data-is-selected="N" onClick={onBtnTypeHawaiiClick}> Chọn </button>
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="col-sm-4">
                    <div className="card w-100">
                      <img src="pizza_type_hai_san.jpg" alt="pizza-img" className="card-img-top" />
                      <div className="card-body">
                        <h3 id="h3-haisan">Pizza Hải sản</h3>
                        <p>Món ăn đến từ biển</p>
                        <p>
                          Bạn đã thử pizza được chế biến từ nguyên liệu hải sản chưa ?
                        </p>
                        <p>
                          <button style={styleSeafood?{background:"orange"}:{background:"#00628f"}} className="btn-green"  id="btn-type-hai-san"
                            data-is-selected="N" onClick={onBtnTypeSeafoodClick}> Chọn </button>
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="col-sm-4">
                    <div className="card w-100">
                      <img src="pizza_type_thit_nuong.jpg" alt="pizza-img" className="card-img-top" />
                      <div className="card-body">
                        <h3 id="h3-bacon">Pizza Bacon</h3>
                        <p>Món ăn đăc biệt</p>
                        <p>
                          Được chế biến từ thịt bacon. Mang đến hương vị mới lạ.
                        </p>
                        <p>
                          <button style={styleBacon?{background:"orange"}:{background:"#00628f"}} className="btn-green"  id="btn-type-bacon"
                            data-is-selected="N" onClick={onBtnTypeBaconClick} > Chọn </button>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            </>
        )
    }
export default Type