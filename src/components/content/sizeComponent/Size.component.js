import { chooseLargeSize, chooseMediumSize, chooseSmallSize } from "../../../action/size.action";
import { SmallSizeInfo,MediumSizeInfo,LargeSizeInfo } from "../../../constants/constants";

import { useDispatch, useSelector } from "react-redux";

const Size = () => {
  const dispatch = useDispatch()
  const { styleL , styleM, styleS } = useSelector((reduxData) => reduxData.pizzaReducer)
  const onBtnSmallSizeClick=()=>{
    dispatch(chooseSmallSize())
  }
  const onBtnMediumSizeClick=()=>{
    dispatch(chooseMediumSize())
  }
  const onBtnLargeSizeClick=()=>{
    dispatch(chooseLargeSize())
  }
  return (
    <>
      <div id="combo" className="row">
        {/* <!-- Title Size Combos --> */}
        <div className="col-sm-12 text-center p-4 mt-4">
          <h2><b className="p-1 border-bottom">Menu combo Pizza 365</b></h2>
          <p><span className="p-2">Hãy chọn combo phù hợp với bạn</span></p>
        </div>
        {/* <!-- Content các Size Combos --> */}
        <div className="col-sm-12">
          <div className="row" id="row-combo">
            <div className="col-sm-4">
              <div className="card">
                <div style={{ background: "#0f6ba8" }} className="card-header bg-main-brand text-white text-center">
                  <h3 id="combo-heading">{SmallSizeInfo.kichCo}</h3>
                </div>
                <div className="card-body text-center">
                  <ul className="list-group list-group-flush">
                    <li className="list-group-item">
                      Đường kính <b id="combo-size">{SmallSizeInfo.duongKinh}</b>
                    </li> 
                    <li className="list-group-item">Sườn nướng <b id="combo-suon">{SmallSizeInfo.suon}</b></li>
                    <li className="list-group-item">Salad <b id="combo-salad">{SmallSizeInfo.salad} gr</b ></li>
                    <li className="list-group-item">Nước ngọt <b id="combo-drink">{SmallSizeInfo.soluongNuoc}</b></li>
                    <li className="list-group-item">
                      <h1>VND <b id="combo-price">{SmallSizeInfo.priceVND}</b></h1>
                    </li>
                  </ul>
                </div>
                <div className="card-footer text-center">
                  <button style={styleS?{background:"orange"}:{background:"#00628f"}} onClick={onBtnSmallSizeClick} 
                  className="btn-green" id="btn-size-small"  data-is-selected="N">
                    Chọn
                  </button>
                </div>
              </div>
            </div>
            {/* <!-- Call Api --> */}
            <div className="col-sm-4">
              <div className="card">
                <div style={{ background: "#0f6ba8" }} className="card-header bg-main-brand text-white text-center">
                  <h3>{MediumSizeInfo.kichCo}</h3>
                </div>
                <div className="card-body text-center">
                  <ul className="list-group list-group-flush">
                    <li className="list-group-item">
                      Đường kính <b>{MediumSizeInfo.duongKinh}</b>
                    </li>
                    <li className="list-group-item">Sườn nướng <b>{MediumSizeInfo.suon}</b></li>
                    <li className="list-group-item">Salad <b>{MediumSizeInfo.salad} gr</b></li>
                    <li className="list-group-item">Nước ngọt <b>{MediumSizeInfo.soluongNuoc}</b></li>
                    <li className="list-group-item">
                      <h1>VND <b>{MediumSizeInfo.priceVND}</b></h1>
                    </li>
                  </ul>
                </div>
                <div className="card-footer text-center">
                  <button style={styleM?{background:"orange"}:{background:"#00628f"}} className="btn-green"
                    id="btn-size-medium" onClick={onBtnMediumSizeClick} data-is-selected="N"> Chọn </button>
                </div>
              </div>
            </div>
            <div className="col-sm-4">
              <div className="card">
                <div style={{ background: "#0f6ba8" }} className="card-header bg-main-brand text-white text-center">
                  <h3>{LargeSizeInfo.kichCo}</h3>
                </div>
                <div className="card-body text-center">
                  <ul className="list-group list-group-flush">
                    <li className="list-group-item">
                      Đường kính <b>{LargeSizeInfo.duongKinh}</b>
                    </li>
                    <li className="list-group-item">Sườn nướng <b>{LargeSizeInfo.suon}</b></li>
                    <li className="list-group-item">Salad <b>{LargeSizeInfo.salad} gr</b></li>
                    <li className="list-group-item">Nước ngọt <b>{LargeSizeInfo.soluongNuoc}</b></li>
                    <li className="list-group-item">
                      <h1>VND <b>{LargeSizeInfo.priceVND}</b></h1>
                    </li>
                  </ul>
                </div>
                <div className="card-footer text-center">
                  <button style={styleL?{background:"orange"}:{background:"#00628f"}} className="btn-green" id="btn-size-large" data-is-selected="N" onClick={onBtnLargeSizeClick}>
                    Chọn </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}
export default Size