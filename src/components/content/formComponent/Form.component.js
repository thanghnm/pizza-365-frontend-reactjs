import { useDispatch, useSelector } from "react-redux";
import {
  changeAddressAction, changeEmailAction, changeMessageAction, changeNameAction, changePhoneAction, changeVoucherAction,
  checkOrderAction, fetchVoucher, postOrder,
} from "../../../action/form.action";

const Form = () => {
  const dispatch = useDispatch()
  const { form, drink, displayCheckOrder, type, size, order, finish, code,
    checked, discount } = useSelector((reduxData) => reduxData.pizzaReducer)
  const onInputNameChangeHandler = (event) => {
    dispatch(changeNameAction(event))
  }
  const onInputEmailChangeHandler = (event) => {
    dispatch(changeEmailAction(event))
  }
  const onInputPhoneChangeHandler = (event) => {
    dispatch(changePhoneAction(event))
  }
  const onInputAddressChangeHandler = (event) => {
    dispatch(changeAddressAction(event))
  }
  const onInputMessageChangeHandler = (event) => {
    dispatch(changeMessageAction(event))
  }
  const onInputVoucherChangeHandler = (event) => {
    dispatch(changeVoucherAction(event))
  }
  //Hàm kiểm tra email
  const isEmailValidate = (paramEmail) => {
    var vRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return vRegex.test(paramEmail)
  }
  //Hàm kiểm tra SĐT
  const isPhoneValidate = (paramPhone) => {
    var regexPhoneNumber = /(84|0[1|3|7|9])+([0-9]{8})\b/g;
    return paramPhone.match(regexPhoneNumber) ? true : false;
  }
  const onBtnOrderCheckClickHandler = (event) => {
    if (size === null) {
      alert("Bạn chưa chọn size pizza")
      return false
    }
    if (type === null) {
      alert("Bạn chưa chọn loại pizza")
      return false
    }
    if (drink === null) {
      alert("Bạn chưa chọn nước uống")
      return false
    }
    if (form.name === "") {
      alert("Bạn chưa nhập tên")
      return false
    }
    if (!isEmailValidate(form.email)) {
      alert("Email không hợp lệ")
      return false
    }
    if (!isPhoneValidate(form.phone)) {
      alert("Số điện thoại không hợp lệ")
      return false
    }
    if (form.address === "") {
      alert("Bạn chưa nhập địa chỉ")
      return false
    }
    dispatch(fetchVoucher(form.voucher))
    dispatch(checkOrderAction(event))
  }
  const onBtnSendClickHandler = () => {
    dispatch(postOrder(order))
  }
  return (
    <>
      <div id="contact" className="row">
        {/* <!-- Title Form Contact Us --> */}
        <div className="col-sm-12 text-center p-4 mt-4">
          <h2><b className="p-2 border-bottom">Thông tin đơn hàng</b></h2>
        </div>
        {/* <!-- Content Form Contact Us --> */}
        <div id="div-contact" className="col-sm-12 p-2 jumbotron">
          <div className="row form-contact">
            <div className="col-sm-12">
              <div className="form-group">
                <label htmlFor="input-name">Họ và tên</label>
                <input value={form.name} onChange={onInputNameChangeHandler} type="text" className="form-control" id="input-name" placeholder="Họ và tên" />
              </div>
              <div className="form-group">
                <label htmlFor="email">Email</label>
                <input value={form.email} onChange={onInputEmailChangeHandler} type="text" className="form-control" id="input-email" placeholder="Email" />
              </div>
              <div className="form-group">
                <label htmlFor="input-phone">Điện thoại</label>
                <input value={form.phone} onChange={onInputPhoneChangeHandler} type="text" className="form-control" id="input-phone" placeholder="Điện thoại" />
              </div>
              <div className="form-group">
                <label htmlFor="input-address">Địa chỉ</label>
                <input value={form.address} onChange={onInputAddressChangeHandler} type="text" className="form-control" id="input-address" placeholder="Địa chỉ" />
              </div>
              <div className="form-group">
                <label htmlFor="message">Lời nhắn</label>
                <input value={form.message} onChange={onInputMessageChangeHandler} type="text" className="form-control" id="input-message" placeholder="Lời nhắn" />
              </div>
              <div className="form-group">
                <label htmlFor="voucher">Voucher</label>
                <input value={form.voucher} onChange={onInputVoucherChangeHandler} type="text" className="form-control" id="input-voucher" placeholder="Mã voucher" />
              </div>
              <button onClick={onBtnOrderCheckClickHandler} style={{ marginTop: 20 }} type="button" className="btn-green"> Kiểm tra đơn </button>
            </div>
          </div>
        </div>
        {/* <!-- vùng hiển thị thông tin đơn hàng(order) --> */}
        {checked ?
          <div style={displayCheckOrder} id="div-container-order" className="container bg-info p-2 jumbotron" >
            <div id="div-order-infor" className="text-white p-3">
              <p>Họ và tên: {form.name}</p>
              <p> Email: {form.email}</p>
              <p> Số điện thoại: {form.phone}</p>
              <p> Địa chỉ : {form.address}</p>
              <p> Lời nhắn: {form.message}</p>
              <hr />
              <p>  Kích cỡ: {size.kichCo}</p>
              <p>  Đường kính: {size.duongKinh}</p>
              <p>  Sườn nướng: {size.suon}</p>
              <p>  Salad : {size.salad} gr</p>
              <p>  Nước ngọt: {size.soLuongNuoc}</p>
              <p> Loại nước: {drink}</p>
              <hr />
              <p>  Loại Pizza: {type}</p>
              <p> Mã voucher: {form.voucher}</p>
              <p> Giá Vnd: {size.priceVND}</p>
              {discount ?
                <>
                  <p> Discount: {discount}%</p>
                  <p> Phải thanh toán VNĐ: {size.priceVND * (100 - discount) / 100}</p>
                </>
                : ""
              }
            </div>
            <div className="p-2">
              <button onClick={onBtnSendClickHandler} type="button" className="btn-yellow"> Gửi đơn </button>
            </div>
          </div>
          : ""}
        {finish ? <div className="thanks"><p>Cảm ơn bạn đã đặt hàng tại Pizza 365. Mã đơn hàng của bạn là: </p>
          <div>Mã đơn hàng:<span className="ordercode">${code}</span></div></div> : ""}
      </div>
    </>
  )
}
export default Form