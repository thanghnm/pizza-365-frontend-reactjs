import { Component } from "react";
import Type from "./typeComponent/Type.component";
import Drink from "./drinkComponent/Drink.component";
import Form from "./formComponent/Form.component";
import Introduce from "./introduceComponent/Introduce.component";
import Size from "./sizeComponent/Size.component";

class Content extends Component{
    render(){
        return(
            <div id="div-container" className="container">
                <Introduce></Introduce>
                <Size></Size>
                <Type></Type>
                <Drink></Drink>
                <Form></Form>
            </div>
        )
    }
}
export default Content