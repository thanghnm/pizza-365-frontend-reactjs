import { LargeSizeInfo, MediumSizeInfo, SIZE_L, SIZE_M, SIZE_S, SmallSizeInfo,  } from "../constants/constants"

export const chooseSmallSize = () =>{
    return(
        {
            type:SIZE_S,
            payload:SmallSizeInfo
          }
    )
}
export const chooseMediumSize = () =>{
    return(
        {
            type:SIZE_M,
            payload:MediumSizeInfo
          }
    )
}
export const chooseLargeSize = () =>{
    return(
        {
            type:SIZE_L,
            payload:LargeSizeInfo
          }
    )
}