import { CHANGE_DRINK, DRINK_FETCH_ERROR, DRINK_FETCH_PENDING, DRINK_FETCH_SUCCESS } from "../constants/constants";

export const fetchDrink =()=>{
    return async (dispatch)=>{
        try {
            var requestOptions = {
                method: 'GET',
                redirect: 'follow'
            };
    
            await dispatch({
                type: DRINK_FETCH_PENDING
            })

            const responseData = await fetch("http://localhost:8000/api/devcamp-pizza365/drinks",requestOptions)

            const data = await responseData.json();
            return dispatch({
                type: DRINK_FETCH_SUCCESS,
                payload: data,
            })
        } catch (error) {
            return dispatch({
                type: DRINK_FETCH_ERROR,
                error: error
            })

        }
    }
}
export const changeDrink = (event)=>{
   return {
        type:CHANGE_DRINK,
        payload:event.target.value
      }
}