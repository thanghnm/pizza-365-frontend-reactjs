import {
    CHANGE_ADDRESS, CHANGE_EMAIL, CHANGE_MESSAGE, CHANGE_NAME, CHANGE_PHONE, CHANGE_VOUCHER,
    CHECK_ORDER, POST_ORDER_ERROR, POST_ORDER_SUCCESS, VOUCHER_FETCH_ERROR, VOUCHER_FETCH_SUCCESS, VOUCHER_NOT_FOUND
} from "../constants/constants"

export const changeNameAction = (event) => {
    return {
        type: CHANGE_NAME,
        payload: event.target.value
    }
}
export const changeEmailAction = (event) => {
    return {
        type: CHANGE_EMAIL,
        payload: event.target.value
    }
}
export const changePhoneAction = (event) => {
    return {
        type: CHANGE_PHONE,
        payload: event.target.value
    }
}
export const changeAddressAction = (event) => {
    return {
        type: CHANGE_ADDRESS,
        payload: event.target.value
    }
}
export const changeMessageAction = (event) => {
    return {
        type: CHANGE_MESSAGE,
        payload: event.target.value
    }
}
export const changeVoucherAction = (event) => {
    return {
        type: CHANGE_VOUCHER,
        payload: event.target.value
    }
}
export const fetchVoucher = (inputVoucher) => {
    return async (dispatch) => {
        try {
            var requestOptions = {
                method: 'GET',
                redirect: 'follow',
            };

            // await dispatch({
            //     type: VOUCHER_FETCH_PENDING
            // })
            // const params = new URLSearchParams({
            //     maVoucher: inputVoucher,
            // });
            const responseData = await fetch("http://localhost:8000/api/devcamp-pizza365/vouchers/" + inputVoucher, requestOptions)
            if (responseData.status === 200) {
                const data = await responseData.json();
                return dispatch({
                    type: VOUCHER_FETCH_SUCCESS,
                    payload: data.result,
                })
            }
            else if (responseData.status === 404) {
                alert("Không tìm thấy mã giảm giá")
                return dispatch({
                    type: VOUCHER_NOT_FOUND,
                    error: responseData.status
                })
            }
        } catch (error) {
            return dispatch({
                type: VOUCHER_FETCH_ERROR,
                error: error.message
            })

        }
    }
}
export const checkOrderAction = (event) => {
    return {
        type: CHECK_ORDER,
    }
}
export const postOrder = (inputOrder) => {
    return async (dispatch) => {
        try {
            var requestOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(inputOrder)
            };
            const responseData = await fetch("http://localhost:8000/api/devcamp-pizza365/orders/", requestOptions)
            if (responseData.status === 201) {
                const data = await responseData.json();
                return dispatch({
                    type: POST_ORDER_SUCCESS,
                    payload: data,
                })
            }
        } catch (error) {
            return dispatch({
                type: POST_ORDER_ERROR,
                error: error.message,

            })

        }
    }
}