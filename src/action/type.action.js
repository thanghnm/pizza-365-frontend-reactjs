import { TYPE_BACON, TYPE_HAWAII, TYPE_SEAFOOD } from "../constants/constants"

export const choosePizzaHawaii = () =>{
    return(
        {
            type:TYPE_HAWAII,
            payload:"Pizza Hawaii"
          }
    )
}
export const choosePizzaSeafood = () =>{
    return(
        {
            type:TYPE_SEAFOOD,
            payload:"Pizza Seafood"
          }
    )
}
export const choosePizzaBacon = () =>{
    return(
        {
            type:TYPE_BACON,
            payload:"Pizza Bacon"
          }
    )
}